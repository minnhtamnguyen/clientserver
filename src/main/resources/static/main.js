(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/activity-list/activity-list.component.css":
/*!***********************************************************!*\
  !*** ./src/app/activity-list/activity-list.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGl2aXR5LWxpc3QvYWN0aXZpdHktbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/activity-list/activity-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/activity-list/activity-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <app-sidebar class=\"sticky\" id=\"sidebar-size\"></app-sidebar>\n  <div class=\"page-detail\">\n    <div class=\"project\">\n      <div class=\"project-title\">\n\n        <div style=\"display: flex; flex-flow: column; max-width: 100%;\">\n\n          <h4 class=\"home\">List of Activities</h4>\n          \n          <div style=\"margin-top: 1% ;\">\n          <table class=\"table\" style=\"width: 90%;\">\n            <thead class=\"thead-dark\">\n              <tr>\n                <th scope=\"col\">#</th>\n                <th scope=\"col\">Project name</th>\n                <th scope=\"col\">From</th>\n                <th scope=\"col\">Status</th>\n              </tr>\n            </thead>\n            <!-- <tbody>\n              <tr *ngFor=\"let task of tasks | async; let i = index;\" [routerLink]='\"/taskDetail/\" + task.id' style=\"cursor: pointer;\">\n                <th scope=\"row\">{{i}}</th>\n                <td>{{task.name}}</td>\n                <td>{{task.startDate}}</td>\n                <td>{{task.status}}</td>\n              </tr>\n            </tbody> -->\n          </table>\n        </div>\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/activity-list/activity-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/activity-list/activity-list.component.ts ***!
  \**********************************************************/
/*! exports provided: ActivityListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityListComponent", function() { return ActivityListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ActivityListComponent = /** @class */ (function () {
    function ActivityListComponent() {
    }
    ActivityListComponent.prototype.ngOnInit = function () {
    };
    ActivityListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-activity-list',
            template: __webpack_require__(/*! ./activity-list.component.html */ "./src/app/activity-list/activity-list.component.html"),
            styles: [__webpack_require__(/*! ./activity-list.component.css */ "./src/app/activity-list/activity-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ActivityListComponent);
    return ActivityListComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/list.component */ "./src/app/list/list.component.ts");
/* harmony import */ var _project_list_project_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./project-list/project-list.component */ "./src/app/project-list/project-list.component.ts");
/* harmony import */ var _project_info_project_info_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./project-info/project-info.component */ "./src/app/project-info/project-info.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _project_detail_project_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./project-detail/project-detail.component */ "./src/app/project-detail/project-detail.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./task-detail/task-detail.component */ "./src/app/task-detail/task-detail.component.ts");
/* harmony import */ var _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./task-list/task-list.component */ "./src/app/task-list/task-list.component.ts");
/* harmony import */ var _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./create-project/create-project.component */ "./src/app/create-project/create-project.component.ts");
/* harmony import */ var _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./create-task/create-task.component */ "./src/app/create-task/create-task.component.ts");
/* harmony import */ var _member_list_member_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./member-list/member-list.component */ "./src/app/member-list/member-list.component.ts");
/* harmony import */ var _activity_list_activity_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./activity-list/activity-list.component */ "./src/app/activity-list/activity-list.component.ts");
















var routes = [
    { path: "list", component: _list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"] },
    { path: "projects", component: _project_list_project_list_component__WEBPACK_IMPORTED_MODULE_4__["ProjectListComponent"] },
    { path: "projects/:projectId", component: _project_info_project_info_component__WEBPACK_IMPORTED_MODULE_5__["ProjectInfoComponent"] },
    { path: "", component: _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] },
    { path: "projectDetail/:projectId", component: _project_detail_project_detail_component__WEBPACK_IMPORTED_MODULE_7__["ProjectDetailComponent"] },
    { path: "header", component: _header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"] },
    { path: "sidebar", component: _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__["SidebarComponent"] },
    { path: "taskDetail/:taskId", component: _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_10__["TaskDetailComponent"] },
    { path: "tasks", component: _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_11__["TaskListComponent"] },
    { path: "createProject", component: _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_12__["CreateProjectComponent"] },
    { path: "createTask", component: _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_13__["CreateTaskComponent"] },
    { path: "members", component: _member_list_member_list_component__WEBPACK_IMPORTED_MODULE_14__["MemberListComponent"] },
    { path: "activities", component: _activity_list_activity_list_component__WEBPACK_IMPORTED_MODULE_15__["ActivityListComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-service.service */ "./src/app/user-service.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(userService) {
        this.userService = userService;
        this.title = 'angular-apollo';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_service_service__WEBPACK_IMPORTED_MODULE_2__["UserServiceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-angular-link-http */ "./node_modules/apollo-angular-link-http/fesm5/ngApolloLinkHttp.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! apollo-cache-inmemory */ "./node_modules/apollo-cache-inmemory/lib/bundle.esm.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./list/list.component */ "./src/app/list/list.component.ts");
/* harmony import */ var _project_list_project_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./project-list/project-list.component */ "./src/app/project-list/project-list.component.ts");
/* harmony import */ var _project_info_project_info_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./project-info/project-info.component */ "./src/app/project-info/project-info.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _project_detail_project_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./project-detail/project-detail.component */ "./src/app/project-detail/project-detail.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./task-detail/task-detail.component */ "./src/app/task-detail/task-detail.component.ts");
/* harmony import */ var _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./task-list/task-list.component */ "./src/app/task-list/task-list.component.ts");
/* harmony import */ var _member_list_member_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./member-list/member-list.component */ "./src/app/member-list/member-list.component.ts");
/* harmony import */ var _member_detail_member_detail_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./member-detail/member-detail.component */ "./src/app/member-detail/member-detail.component.ts");
/* harmony import */ var ngx_moment__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-moment */ "./node_modules/ngx-moment/fesm5/ngx-moment.js");
/* harmony import */ var _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./create-project/create-project.component */ "./src/app/create-project/create-project.component.ts");
/* harmony import */ var _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./create-task/create-task.component */ "./src/app/create-task/create-task.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _activity_list_activity_list_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./activity-list/activity-list.component */ "./src/app/activity-list/activity-list.component.ts");

























var AppModule = /** @class */ (function () {
    function AppModule(apollo, httpLink) {
        apollo.create({
            link: httpLink.create({ uri: 'http://localhost:8082/graphql' }),
            cache: new apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_7__["InMemoryCache"]()
        });
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_9__["ListComponent"],
                _project_list_project_list_component__WEBPACK_IMPORTED_MODULE_10__["ProjectListComponent"],
                _project_info_project_info_component__WEBPACK_IMPORTED_MODULE_11__["ProjectInfoComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_12__["HomeComponent"],
                _project_detail_project_detail_component__WEBPACK_IMPORTED_MODULE_13__["ProjectDetailComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_14__["HeaderComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_15__["SidebarComponent"],
                _task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_16__["TaskDetailComponent"],
                _task_list_task_list_component__WEBPACK_IMPORTED_MODULE_17__["TaskListComponent"],
                _member_list_member_list_component__WEBPACK_IMPORTED_MODULE_18__["MemberListComponent"],
                _member_detail_member_detail_component__WEBPACK_IMPORTED_MODULE_19__["MemberDetailComponent"],
                _create_project_create_project_component__WEBPACK_IMPORTED_MODULE_21__["CreateProjectComponent"],
                _create_task_create_task_component__WEBPACK_IMPORTED_MODULE_22__["CreateTaskComponent"],
                _activity_list_activity_list_component__WEBPACK_IMPORTED_MODULE_24__["ActivityListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                apollo_angular__WEBPACK_IMPORTED_MODULE_4__["ApolloModule"],
                apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_5__["HttpLinkModule"],
                ngx_moment__WEBPACK_IMPORTED_MODULE_20__["MomentModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_23__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_23__["ReactiveFormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_4__["Apollo"],
            apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_5__["HttpLink"]])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/create-project/create-project.component.css":
/*!*************************************************************!*\
  !*** ./src/app/create-project/create-project.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NyZWF0ZS1wcm9qZWN0L2NyZWF0ZS1wcm9qZWN0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/create-project/create-project.component.html":
/*!**************************************************************!*\
  !*** ./src/app/create-project/create-project.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"formdata\">\n  <div class=\"modal-header\">\n    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Create Project</h5>\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"form-group\">\n      <label for=\"formGroupExampleInput\">Project's Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"formGroupExampleInput\" placeholder=\"Enter project's name\"\n        formControlName=\"projectName\" required>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"formGroupExampleInput\">Project's Code</label>\n      <input type=\"text\" class=\"form-control\" id=\"formGroupExampleInput\" placeholder=\"Enter project's code\"\n        formControlName=\"projectCode\" required>\n      <small id=\"emailHelp\" class=\"form-text text-muted\">Project's code should be unique.</small>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleFormControlTextarea1\">Description</label>\n      <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\" formControlName=\"projectDesc\"\n        required></textarea>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Start Day</label>\n      <input type=\"date\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\"\n        >\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Deadline</label>\n      <input type=\"date\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\"\n        >\n    </div>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"createProject()\">Create</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/create-project/create-project.component.ts":
/*!************************************************************!*\
  !*** ./src/app/create-project/create-project.component.ts ***!
  \************************************************************/
/*! exports provided: CreateProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProjectComponent", function() { return CreateProjectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_4__);





var CreateProjectComponent = /** @class */ (function () {
    function CreateProjectComponent(apollo) {
        this.apollo = apollo;
    }
    CreateProjectComponent.prototype.ngOnInit = function () {
        this.formdata = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            projectName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            projectCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            projectDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
        });
    };
    CreateProjectComponent.prototype.createProject = function () {
        var _this = this;
        var inputData = {
            name: this.formdata.value["projectName"],
            projectCode: this.formdata.value["projectCode"],
            description: this.formdata.value["projectDesc"],
        };
        console.log(inputData);
        console.log("\n  mutation{\n    createProject(project : {\n      name: " + JSON.stringify(inputData.name) + ",\n      projectCode: " + JSON.stringify(inputData.projectCode) + ",\n      description: " + JSON.stringify(inputData.description) + "\n    }}){\n\n    }\n  }\n  ");
        this.apollo.mutate({
            mutation: graphql_tag__WEBPACK_IMPORTED_MODULE_4___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation{\n      createProject(project : {\n        name: ", ",\n        projectCode: ", ",\n        description: ", "\n      }){\n        name\n        projectCode\n        description\n      }\n    }\n    "], ["\n    mutation{\n      createProject(project : {\n        name: ", ",\n        projectCode: ", ",\n        description: ", "\n      }){\n        name\n        projectCode\n        description\n      }\n    }\n    "])), JSON.stringify(inputData.name), JSON.stringify(inputData.projectCode), JSON.stringify(inputData.description))
        }).subscribe(function (data) {
            _this.project = data;
            console.log(_this.project);
        }, function (error) {
            console.log("sai");
        });
    };
    CreateProjectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-create-project',
            template: __webpack_require__(/*! ./create-project.component.html */ "./src/app/create-project/create-project.component.html"),
            styles: [__webpack_require__(/*! ./create-project.component.css */ "./src/app/create-project/create-project.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], CreateProjectComponent);
    return CreateProjectComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/create-task/create-task.component.css":
/*!*******************************************************!*\
  !*** ./src/app/create-task/create-task.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NyZWF0ZS10YXNrL2NyZWF0ZS10YXNrLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/create-task/create-task.component.html":
/*!********************************************************!*\
  !*** ./src/app/create-task/create-task.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"formdata\">\n  <div class=\"modal-header\">\n    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Create Task</h5>\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"form-group\">\n      <label for=\"formGroupExampleInput\">Task's Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"formGroupExampleInput\" placeholder=\"Enter task's name\"\n        formControlName=\"taskName\" required>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"formGroupExampleInput\">Task's Code</label>\n      <input type=\"text\" class=\"form-control\" id=\"formGroupExampleInput\" placeholder=\"Enter task's code\"\n        formControlName=\"taskCode\" required>\n      <small id=\"emailHelp\" class=\"form-text text-muted\">Task's code should be unique.</small>\n    </div>\n    <div class=\"form-group\">\n    <label for=\"formGroupExampleInput\">Project's Code</label>\n    <input type=\"text\" class=\"form-control\" id=\"formGroupExampleInput\" formControlName=\"projectCode\"\n        readonly>\n      </div>\n    <div class=\"form-group\">\n      <label for=\"exampleFormControlTextarea1\">Description</label>\n      <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\" formControlName=\"taskDesc\"\n        required></textarea>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Start Day</label>\n      <input type=\"date\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\"\n        formControlName=\"taskStart\" required>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Deadline</label>\n      <input type=\"date\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\"\n        formControlName=\"taskDeadline\" required>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleFormControlSelect1\">Report to:</label>\n      <select class=\"form-control\" id=\"exampleFormControlSelect1\">\n        <option>1</option>\n        <option>2</option>\n        <option>3</option>\n        <option>4</option>\n        <option>5</option>\n      </select>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleFormControlSelect1\">Assign to:</label>\n      <select class=\"form-control\" id=\"exampleFormControlSelect1\">\n        <option>1</option>\n        <option>2</option>\n        <option>3</option>\n        <option>4</option>\n        <option>5</option>\n      </select>\n    </div>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n    <button type=\"button\" class=\"btn btn-primary\">Create</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/create-task/create-task.component.ts":
/*!******************************************************!*\
  !*** ./src/app/create-task/create-task.component.ts ***!
  \******************************************************/
/*! exports provided: CreateTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTaskComponent", function() { return CreateTaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_4__);





var CreateTaskComponent = /** @class */ (function () {
    function CreateTaskComponent(apollo) {
        this.apollo = apollo;
        this.huhu = "haha";
    }
    CreateTaskComponent.prototype.ngOnInit = function () {
        this.formdata = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            taskName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            taskCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            taskDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            projectCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.code, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
            // taskStart: new FormControl("", Validators.required),
            // taskDeadline: new FormControl("", Validators.required),
        });
    };
    CreateTaskComponent.prototype.createtask = function () {
        var _this = this;
        var inputData = {
            name: this.formdata.value["taskName"],
            taskCode: this.formdata.value["taskCode"],
            description: this.formdata.value["taskDesc"],
            projectCode: this.formdata.value["projectCode"],
        };
        console.log("\n    mutation{\n      createtask(task :{\n        name:   " + JSON.stringify(inputData.name) + "\n        taskCode:  " + JSON.stringify(inputData.name) + "\n        description:  " + JSON.stringify(inputData.name) + "\n      })\n    ");
        this.apollo.mutate({
            mutation: graphql_tag__WEBPACK_IMPORTED_MODULE_4___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      mutation{\n        createtask(task : \n          \n        })\n      }\n      "], ["\n      mutation{\n        createtask(task : \n          \n        })\n      }\n      "])))
        }).subscribe(function (data) {
            _this.task = data;
        }, function (error) {
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CreateTaskComponent.prototype, "code", void 0);
    CreateTaskComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-create-task',
            template: __webpack_require__(/*! ./create-task.component.html */ "./src/app/create-task/create-task.component.html"),
            styles: [__webpack_require__(/*! ./create-task.component.css */ "./src/app/create-task/create-task.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], CreateTaskComponent);
    return CreateTaskComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sticky header\">\n  <nav class=\"navbar navbar-expand-lg navbar-light\">\n\n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" >\n\n      <i class=\"fa fa-home text-secondary fa-2x pointer\" style=\"margin-right: 1%;\" aria-hidden=\"true\" [routerLink]=\"'/'\"></i>\n      <div class=\"search\">\n        <form class=\"form-inline my-2 my-lg-0\">\n          <input class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\">\n        </form>\n          <i class=\"fa fa-search search-icon\" [routerLink] aria-hidden=\"true\"></i>\n      </div>\n      <button type=\"button\" class=\"btn btn-outline-success my-2 my-sm-0 sign-in\"><i class=\"fa fa-user-circle-o\" aria-hidden=\"true\"></i>  Sign in</button>\n\n    </div>\n  </nav>\n</div>"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <div class=\"home-content\">\n    <h4 class=\"home\">Waiting project for approval\n      <a (click)=\"refresh()\"><i class=\"fa fa-refresh pointer\" aria-hidden=\"true\"></i></a>\n    </h4>  \n    <a (click)=\"test()\"><i class=\"fa fa-refresh pointer\" aria-hidden=\"true\"></i></a>\n    <div class=\"project-list-home \">\n      <div class=\"project-folder-home\" *ngFor=\"let project of projects | async\"\n        [routerLink]='\"/projectDetail/\" + project.id'>\n        <div>\n          <i class=\"fa fa-folder-o fa-5x\" aria-hidden=\"true\"></i>\n        </div>\n        <div>\n          <span>{{project.name}}</span>\n        </div>\n      </div>\n    </div>\n    <hr class=\"home\">\n    <h4 class=\"home\">Present project\n      <a (click)=\"refresh()\"><i class=\"fa fa-refresh pointer\" aria-hidden=\"true\"></i></a>\n    </h4>\n    <div class=\"project-list-home\">\n\n      <div class=\"project-folder-home\">\n        <div data-target=\"exampleModal\">\n          <!-- <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n            Launch demo modal\n          </button> -->\n          <i class=\"fa fa-plus-square fa-5x text-success\" aria-hidden=\"true\"  data-toggle=\"modal\" data-target=\"#exampleModal\"></i>\n        </div>\n        <div>\n          <span>New project</span>\n        </div>\n      </div>\n      <div class=\"project-folder-home\">\n        <div>\n          <i class=\"fa fa-folder-o fa-5x\" aria-hidden=\"true\"></i>\n        </div>\n        <div>\n          <span>Project name</span>\n        </div>\n      </div>\n\n    </div>\n    <hr class=\"home\">\n    <h4 class=\"home\">Completed project\n      <a (click)=\"refresh()\"><i class=\"fa fa-refresh pointer\" aria-hidden=\"true\"></i></a>\n    </h4>\n    <div class=\"project-list-home\">\n      <div class=\"project-folder-home\">\n        <div>\n          <i class=\"fa fa-folder-o fa-5x\" aria-hidden=\"true\"></i>\n        </div>\n        <div>\n          <span>Project name</span>\n        </div>\n      </div>\n      <div class=\"project-folder-home\">\n        <div>\n          <i class=\"fa fa-folder-o fa-5x\" aria-hidden=\"true\"></i>\n        </div>\n        <div>\n          <span>Project name</span>\n        </div>\n      </div>\n\n    </div>\n    <hr class=\"home\">\n    <h4 class=\"home\">Company's project\n      <a (click)=\"refresh()\"><i class=\"fa fa-refresh pointer\" aria-hidden=\"true\"></i></a>\n    </h4>\n    <div class=\"project-list-home\">\n      <div class=\"project-folder-home\">\n        <div>\n          <i class=\"fa fa-folder-o fa-5x\" aria-hidden=\"true\"></i>\n        </div>\n        <div>\n          <span>Project name</span>\n        </div>\n      </div>\n      \n    </div>\n  </div>\n</div>\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <app-create-project></app-create-project>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _user_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../user-service.service */ "./src/app/user-service.service.ts");






var HomeComponent = /** @class */ (function () {
    function HomeComponent(apollo, userService) {
        this.apollo = apollo;
        this.userService = userService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.refresh();
    };
    HomeComponent.prototype.refresh = function () {
        this.projects = this.apollo.subscribe({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_3___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        allProjects{\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n        }\n      }\n      "], ["\n      query{\n        allProjects{\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n        }\n      }\n      "])))
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) { return result.data['allProjects']; }));
        this.projects.subscribe();
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"],
            _user_service_service__WEBPACK_IMPORTED_MODULE_5__["UserServiceService"]])
    ], HomeComponent);
    return HomeComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/list/list.component.css":
/*!*****************************************!*\
  !*** ./src/app/list/list.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/list/list.component.html":
/*!******************************************!*\
  !*** ./src/app/list/list.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngFor=\"let course of courses | async\">\n  <div class=\"card\" style=\"width: 100%; margin-top: 10px\">\n    <div class=\"card-body\">\n      <h5 class=\"card-title\">{{course.name}}</h5>\n      <h6 class=\"card-subtitle mb-2 text-muted\">by {{course.name}}</h6>\n      <p class=\"card-text\">{{course.id}}</p>\n      <a href=\"{{course.url}}\" class=\"card-link\">Go to course ...</a>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/list/list.component.ts":
/*!****************************************!*\
  !*** ./src/app/list/list.component.ts ***!
  \****************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_4__);





var ListComponent = /** @class */ (function () {
    function ListComponent(apollo) {
        this.apollo = apollo;
    }
    ListComponent.prototype.ngOnInit = function () {
        this.projects = this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_4___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        allProjects{\n          id\n          name\n        }\n      }\n      "], ["\n      query{\n        allProjects{\n          id\n          name\n        }\n      }\n      "])))
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.data['allProjects']; }));
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], ListComponent);
    return ListComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/member-detail/member-detail.component.css":
/*!***********************************************************!*\
  !*** ./src/app/member-detail/member-detail.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbWJlci1kZXRhaWwvbWVtYmVyLWRldGFpbC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/member-detail/member-detail.component.html":
/*!************************************************************!*\
  !*** ./src/app/member-detail/member-detail.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  member-detail works!\n</p>\n"

/***/ }),

/***/ "./src/app/member-detail/member-detail.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/member-detail/member-detail.component.ts ***!
  \**********************************************************/
/*! exports provided: MemberDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberDetailComponent", function() { return MemberDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MemberDetailComponent = /** @class */ (function () {
    function MemberDetailComponent() {
    }
    MemberDetailComponent.prototype.ngOnInit = function () {
    };
    MemberDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-member-detail',
            template: __webpack_require__(/*! ./member-detail.component.html */ "./src/app/member-detail/member-detail.component.html"),
            styles: [__webpack_require__(/*! ./member-detail.component.css */ "./src/app/member-detail/member-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MemberDetailComponent);
    return MemberDetailComponent;
}());



/***/ }),

/***/ "./src/app/member-list/member-list.component.css":
/*!*******************************************************!*\
  !*** ./src/app/member-list/member-list.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbWJlci1saXN0L21lbWJlci1saXN0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/member-list/member-list.component.html":
/*!********************************************************!*\
  !*** ./src/app/member-list/member-list.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <app-sidebar class=\"sticky\" id=\"sidebar-size\"></app-sidebar>\n  <div class=\"page-detail\">\n    <div class=\"project\">\n      <div class=\"project-title\">\n\n        <div style=\"display: flex; flex-flow: column; max-width: 100%;\">\n\n          <h4 class=\"home\">List of Members</h4>\n          \n          <div style=\"margin-top: 1% ;\">\n          <table class=\"table\" style=\"width: 90%;\">\n            <thead class=\"thead-dark\">\n              <tr>\n                <th scope=\"col\">#</th>\n                <th scope=\"col\">Project name</th>\n                <th scope=\"col\">From</th>\n                <th scope=\"col\">Status</th>\n              </tr>\n            </thead>\n            <!-- <tbody>\n              <tr *ngFor=\"let task of tasks | async; let i = index;\" [routerLink]='\"/taskDetail/\" + task.id' style=\"cursor: pointer;\">\n                <th scope=\"row\">{{i}}</th>\n                <td>{{task.name}}</td>\n                <td>{{task.startDate}}</td>\n                <td>{{task.status}}</td>\n              </tr>\n            </tbody> -->\n          </table>\n        </div>\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/member-list/member-list.component.ts":
/*!******************************************************!*\
  !*** ./src/app/member-list/member-list.component.ts ***!
  \******************************************************/
/*! exports provided: MemberListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberListComponent", function() { return MemberListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MemberListComponent = /** @class */ (function () {
    function MemberListComponent() {
    }
    MemberListComponent.prototype.ngOnInit = function () {
    };
    MemberListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-member-list',
            template: __webpack_require__(/*! ./member-list.component.html */ "./src/app/member-list/member-list.component.html"),
            styles: [__webpack_require__(/*! ./member-list.component.css */ "./src/app/member-list/member-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MemberListComponent);
    return MemberListComponent;
}());



/***/ }),

/***/ "./src/app/project-detail/project-detail.component.css":
/*!*************************************************************!*\
  !*** ./src/app/project-detail/project-detail.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtZGV0YWlsL3Byb2plY3QtZGV0YWlsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/project-detail/project-detail.component.html":
/*!**************************************************************!*\
  !*** ./src/app/project-detail/project-detail.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <app-sidebar class=\"sticky\" id=\"sidebar-size\"></app-sidebar>\n  <div class=\"page-detail\">\n    <div class=\"project\">\n      <div class=\"project-title\">\n        <div class=\"project-title-content\">\n          <div class=\"project-title-content-name\">\n            <h4 class=\"home\">Name: {{project.name}}</h4>\n            <div style=\"display: flex; flex-flow: row;\">\n              <span class=\"path pointer\" [routerLink]='\"/\"'>...</span>\n              <span>/</span>\n              <span class=\"path pointer\" [routerLink]>projectName-projectCode</span>\n            </div>\n          </div>\n        </div>\n        <div class=\"project-title-content-button\">\n          <span class=\"badge pointer project-title-content-approve-button approve\">Approve</span>\n          <span class=\"badge pointer project-title-content-approve-button decline\">Decline</span>\n        </div>\n        <div class=\"project-tag\">\n          <div class=\"badge badge-primary project-detail-tag\">Members: 9</div>\n          <div class=\"badge badge-info project-detail-tag\">Tasks: 20</div>\n          <div class=\"badge badge-success project-detail-tag\">\n            <span *ngIf=\"project.approve\">Approved</span>\n            <span *ngIf=\"!project.approve\">Pendding</span></div>\n          <div class=\"badge badge-warning project-detail-tag\">Late: 5 days</div>\n        </div>\n      </div>\n      <div class=\"project-classify\">\n        <div class=\"project-content\">\n          <div class=\"project-detail\">\n            <h3 class=\"project-detail-title\">Details</h3>\n            <table class=\"project-detail-table\">\n              <tr>\n                <td>\n                  <span class=\"label\">Project code:</span>\n                  <span>{{project.projectCode}}</span><br>\n                  <span class=\"label\">Name: </span>\n                  <span>{{project.name}}</span><br>\n                  <span class=\"label\">Created date: </span>\n                  <span>{{project.createdDate}}</span><br>\n                  <span class=\"label\">Start date: </span>\n                  <span>{{project.startDate}}</span><br>\n                  <span class=\"label\">Deadline: </span>\n                  <span>{{project.deadline}}</span><br>\n                </td>\n                <td>\n                  <span class=\"label\">State: </span>\n                  <span *ngIf=\"project.approve\" style=\"word-break: break-all\"> Approved</span>\n                  <span *ngIf=\"!project.approve\" style=\"word-break: break-all\">Pendding</span>\n                  <span class=\"label\">Status: </span>\n                  <span>{{project.status}}</span><br>\n                  <span class=\"label\">Author: </span>\n                  <span>Author name</span><br>\n                </td>\n              </tr>\n\n            </table>\n\n          </div>\n\n          <div class=\"project-detail\">\n\n            <h4 class=\"project-detail-title\">Description</h4>\n            <textarea style=\"width: 100%; height: 20%;\"></textarea>\n\n          </div>\n\n          <div class=\"project-detail\">\n\n            <h4 class=\"project-detail-title\">Tasks <i class=\"fa fa-plus-square pointer\" aria-hidden=\"true\" data-toggle=\"modal\" data-target=\"#exampleModal\"></i></h4>\n            \n            <div class=\"task-list\">\n              <table class=\"table table-striped\">\n                <thead>\n                  <tr>\n                    <th>Name</th>\n                    <th>Start day</th>\n                    <th>Code</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  \n                  <tr *ngFor=\"let task of project.tasks\" [routerLink]='\"/taskDetail/\" + task.id' >\n                    <td>{{task.name}}</td>\n                    <td>\n                      {{ 'Dec 12 2020' | amParse: \"MMM dd yyyy\" | amDateFormat:'LL'}}\n                    </td>\n                    <td>{{ task.taskCode}}</td>\n                  </tr>\n                \n                </tbody>\n              </table>\n            </div>\n\n          </div>\n        </div>\n\n        <div class=\"project-content\">\n          <div class=\"project-detail members\">\n            <h4 class=\"project-detail-title\">People</h4>\n            <table class=\"table table-sm\">\n              <thead>\n                <tr>\n                  <th scope=\"col\">#</th>\n                  <th scope=\"col\">Name</th>\n                  <th scope=\"col\">Position</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr>\n                  <th scope=\"row\">1</th>\n                  <td>Mark</td>\n                  <td>analyst</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">2</th>\n                  <td>Jacob</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <app-create-task [code]=\"code\"></app-create-task>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/project-detail/project-detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/project-detail/project-detail.component.ts ***!
  \************************************************************/
/*! exports provided: ProjectDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectDetailComponent", function() { return ProjectDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ProjectDetailComponent = /** @class */ (function () {
    function ProjectDetailComponent(apollo, route) {
        this.apollo = apollo;
        this.route = route;
    }
    ProjectDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (p) {
            _this.projectId = p['projectId'];
        });
        this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_4___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        project(projectId:\"", "\"){\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n          deadline\n          tasks{\n            id\n            name\n            taskCode\n            startDate\n          }\n        }\n      }\n      "], ["\n      query{\n        project(projectId:\"", "\"){\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n          deadline\n          tasks{\n            id\n            name\n            taskCode\n            startDate\n          }\n        }\n      }\n      "])), this.projectId)
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.data['project']; })).subscribe(function (data) {
            _this.project = data;
            _this.code = data.projectCode;
        });
    };
    ProjectDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-detail',
            template: __webpack_require__(/*! ./project-detail.component.html */ "./src/app/project-detail/project-detail.component.html"),
            styles: [__webpack_require__(/*! ./project-detail.component.css */ "./src/app/project-detail/project-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ProjectDetailComponent);
    return ProjectDetailComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/project-info/project-info.component.css":
/*!*********************************************************!*\
  !*** ./src/app/project-info/project-info.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtaW5mby9wcm9qZWN0LWluZm8uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/project-info/project-info.component.html":
/*!**********************************************************!*\
  !*** ./src/app/project-info/project-info.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "{{ project | async | json}}\r\n<table class=\"table\" style=\"width: 500px; margin-top: 10%; margin-left: 30% ; \">\r\n    <thead class=\"thead-dark\">\r\n      <tr>\r\n        <th scope=\"col\">#</th>\r\n        <th scope=\"col\">Project nam e</th>\r\n        <th scope=\"col\">From</th>\r\n        <th scope=\"col\">Status</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor=\"let project of projects | async\" [routerLink]='\"/projects/\" + project.id' style=\"cursor: pointer;\">\r\n        <th scope=\"row\">1</th>\r\n        <td>{{project.name}}</td>\r\n        <td>{{project.startDate}}</td>\r\n        <td>{{project.status}}</td>\r\n      </tr>\r\n    </tbody>"

/***/ }),

/***/ "./src/app/project-info/project-info.component.ts":
/*!********************************************************!*\
  !*** ./src/app/project-info/project-info.component.ts ***!
  \********************************************************/
/*! exports provided: ProjectInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectInfoComponent", function() { return ProjectInfoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ProjectInfoComponent = /** @class */ (function () {
    function ProjectInfoComponent(apollo, route) {
        this.apollo = apollo;
        this.route = route;
    }
    ProjectInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (p) {
            _this.projectId = p['projectId'];
        });
        this.project = this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_3___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        project(projectId:\"", "\"){\n          id\n          name\n  description\n  startDate\n  status\n  approve\n  createdDate\n  projectCode\n  createdBy\n        }\n      }\n      "], ["\n      query{\n        project(projectId:\"", "\"){\n          id\n          name\n  description\n  startDate\n  status\n  approve\n  createdDate\n  projectCode\n  createdBy\n        }\n      }\n      "])), this.projectId)
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) { return result.data['project']; }));
    };
    ProjectInfoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-info',
            template: __webpack_require__(/*! ./project-info.component.html */ "./src/app/project-info/project-info.component.html"),
            styles: [__webpack_require__(/*! ./project-info.component.css */ "./src/app/project-info/project-info.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ProjectInfoComponent);
    return ProjectInfoComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/project-list/project-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/project-list/project-list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtbGlzdC9wcm9qZWN0LWxpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/project-list/project-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/project-list/project-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div *ngFor=\"let project of projects | async\">\n  <div class=\"card\" style=\"width: 100%; margin-top: 10px\">\n    <div class=\"card-body\">\n      <h5 class=\"card-title\">{{project.name}}</h5>\n      <h6 class=\"card-subtitle mb-2 text-muted\">by {{project.name}}</h6>\n      <p class=\"card-text\">{{project.id}}</p>\n    </div>\n  </div>\n</div> -->\n\n<table class=\"table\" style=\"width: 500px; margin-top: 10%; margin-left: 30% ; \">\n  <thead class=\"thead-dark\">\n    <tr>\n      <th scope=\"col\">#</th>\n      <th scope=\"col\">Project name</th>\n      <th scope=\"col\">From</th>\n      <th scope=\"col\">Status</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let project of projects | async\" [routerLink]='\"/projects/\" + project.id' style=\"cursor: pointer;\">\n      <th scope=\"row\">1</th>\n      <td>{{project.name}}</td>\n      <td>{{project.startDate}}</td>\n      <td>{{project.status}}</td>\n    </tr>\n  </tbody>\n</table>\n\n\n\n\n"

/***/ }),

/***/ "./src/app/project-list/project-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/project-list/project-list.component.ts ***!
  \********************************************************/
/*! exports provided: ProjectListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectListComponent", function() { return ProjectListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var ProjectListComponent = /** @class */ (function () {
    function ProjectListComponent(apollo) {
        this.apollo = apollo;
    }
    ProjectListComponent.prototype.ngOnInit = function () {
        this.projects = this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_3___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        allProjects{\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n        }\n      }\n      "], ["\n      query{\n        allProjects{\n          id\n          name\n          description\n          startDate\n          status\n          approve\n          createdDate\n          projectCode\n        }\n      }\n      "])))
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) { return result.data['allProjects']; }));
    };
    ProjectListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-list',
            template: __webpack_require__(/*! ./project-list.component.html */ "./src/app/project-list/project-list.component.html"),
            styles: [__webpack_require__(/*! ./project-list.component.css */ "./src/app/project-list/project-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], ProjectListComponent);
    return ProjectListComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/sidebar/sidebar.component.css":
/*!***********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.html":
/*!************************************************!*\
  !*** ./src/app/sidebar/sidebar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"all-content\"> -->\n<div class=\"wrapper\">\n  <!-- Sidebar -->\n  <nav id=\"sidebar\" style=\"height: 100%;\">\n      <div class=\"sidebar-header\">\n          \n          <div class=\"dropdown\">\n            <button class=\"btn dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n              <h3>Project name</h3>\n            </button>\n            <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\" >\n              <a class=\"dropdown-item\" href=\"#\">Action</a>\n              <a class=\"dropdown-item\" href=\"#\">Another action</a>\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n            </div>\n          </div>\n      </div>\n\n      <ul class=\"list-unstyled components\">\n          <li>\n            <a [routerLink]='\"/projectDetail/\" + projectId'>Summary</a>\n          </li>\n          <li>\n            <a [routerLink]='\"/tasks/\"'>Task</a>\n          </li>\n          <li>\n            <a [routerLink]='\"/members/\"'>Members</a>\n          </li>\n          <li>\n              <a [routerLink]='\"/activities/\"'>Activities</a>\n          </li>\n          <li>\n            <a href=\"#\">Release</a>\n          </li>\n          <li>\n            <a href=\"#\">Report</a>\n          </li>\n      </ul>\n  </nav>\n</div>\n<!-- </div> -->"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(route) {
        this.route = route;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (p) {
            _this.projectId = p['projectId'];
        });
    };
    SidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.css */ "./src/app/sidebar/sidebar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/task-detail/task-detail.component.css":
/*!*******************************************************!*\
  !*** ./src/app/task-detail/task-detail.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2stZGV0YWlsL3Rhc2stZGV0YWlsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/task-detail/task-detail.component.html":
/*!********************************************************!*\
  !*** ./src/app/task-detail/task-detail.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <app-sidebar class=\"sticky\" id=\"sidebar-size\"></app-sidebar>\n  <div class=\"page-detail\">\n    <div class=\"project\">\n      <div class=\"project-title\">\n        <div class=\"project-title-content\">\n          <div class=\"project-title-content-name\">\n            <h4 class=\"home\">Name: {{task.name}}</h4>\n            <div style=\"display: flex; flex-flow: row;\">\n              <span class=\"path pointer\" [routerLink]='\"/\"'>...</span>\n              <span>/</span>\n              <span class=\"path pointer\" [routerLink]>projectName-projectCode</span>\n              <span>/</span>\n              <span class=\"path pointer\" [routerLink]>taskName-taskCode</span>\n            </div>\n          </div>\n        </div>\n        <div class=\"project-title-content-button\">\n          <span class=\"badge pointer project-title-content-approve-button approve\">Approve</span>\n          <span class=\"badge pointer project-title-content-approve-button decline\">Decline</span>\n        </div>\n        <div class=\"project-tag\">\n          <div class=\"badge badge-primary project-detail-tag\">Members: 9</div>\n          <div class=\"badge badge-info project-detail-tag\">Tasks: 20</div>\n          <div class=\"badge badge-success project-detail-tag\">\n            <span *ngIf=\"task.approve\">Approved</span>\n            <span *ngIf=\"!task.approve\">Pendding</span></div>\n          <div class=\"badge badge-warning project-detail-tag\">Late: 5 days</div>\n        </div>\n      </div>\n      <div class=\"project-classify\">\n        <div class=\"project-content\">\n          <div class=\"project-detail\">\n            <h4 class=\"project-detail-title\">Details</h4>\n            <table class=\"project-detail-table\">\n              <tr>\n                <td>\n                  <span class=\"label\">Name: </span>\n                  <span>{{task.name}}</span><br>\n                  <span class=\"label\">Task code: </span>\n                  <span>{{task.taskCode}}</span><br>\n                  <span class=\"label\">Project code: </span>\n                  <span>Project code</span><br>\n                  <span class=\"label\">Created date: </span>\n                  <span>{{task.createdDate}}</span><br>\n                  <span class=\"label\">Start date: </span>\n                  <span>{{task.startDate}}</span><br>\n                  <span class=\"label\">Deadline: </span>\n                  <span>{{task.deadline}}</span><br>\n                </td>\n                <td>\n                  <span class=\"label\">State: </span>\n                  <span *ngIf=\"task.approve\" style=\"word-break: break-all\">Approved</span>\n                  <span *ngIf=\"!task.approve\" style=\"word-break: break-all\">Pendding</span>\n                  <span class=\"label\">Status: </span>\n                  <span>{{task.status}}</span><br>\n                  <span class=\"label\">Reporter: </span>\n                  <span>Author name</span><br>\n                  <span class=\"label\">Assign to: </span>\n                  <span>Author name</span><br>\n                </td>\n              </tr>\n            </table>\n\n          </div>\n\n          <div class=\"project-detail\">\n\n            <h4 class=\"project-detail-title\">Description</h4>\n            <textarea style=\"width: 100%; height: 20%;\"></textarea>\n\n          </div>\n\n          <div class=\"project-detail\">\n\n            <h4 class=\"project-detail-title\">SubTask</h4>\n            <div class=\"task-list\">\n              <table class=\"table table-striped\">\n                <thead>\n                  <tr>\n                    <th>Name</th>\n                    <th>Description</th>\n                    <th>Start day</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr>\n                    <td>Task name</td>\n                    <td>Desciption</td>\n                    <td>20/11/2020</td>\n                  </tr>\n                  <tr>\n                    <td>Task name</td>\n                    <td>Desciption</td>\n                    <td>20/11/2020</td>\n                  </tr>\n                  <tr>\n                    <td>Task name</td>\n                    <td>Desciption</td>\n                    <td>20/11/2020</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n\n          </div>\n        </div>\n\n        <div class=\"project-content\">\n          <div class=\"project-detail members\">\n            <h4 class=\"project-detail-title members\">People</h4>\n            <table class=\"table table-sm\">\n              <thead>\n                <tr>\n                  <th scope=\"col\">#</th>\n                  <th scope=\"col\">Name</th>\n                  <th scope=\"col\">Position</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr>\n                  <th scope=\"row\">1</th>\n                  <td>Mark</td>\n                  <td>analyst</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">2</th>\n                  <td>Jacob</td>\n                  <td>dev</td>\n                </tr>\n                <tr>\n                  <th scope=\"row\">3</th>\n                  <td>Larry the Bird</td>\n                  <td>dev</td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/task-detail/task-detail.component.ts":
/*!******************************************************!*\
  !*** ./src/app/task-detail/task-detail.component.ts ***!
  \******************************************************/
/*! exports provided: TaskDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailComponent", function() { return TaskDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var TaskDetailComponent = /** @class */ (function () {
    function TaskDetailComponent(apollo, route) {
        this.apollo = apollo;
        this.route = route;
    }
    TaskDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (p) {
            _this.taskId = p['taskId'];
        });
        this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_4___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        task(taskId:\"", "\"){\n          id\n          name\n          description\n          startDate\n          deadline\n          status\n          approve\n          taskCode\n          createdDate\n        }\n      }\n      "], ["\n      query{\n        task(taskId:\"", "\"){\n          id\n          name\n          description\n          startDate\n          deadline\n          status\n          approve\n          taskCode\n          createdDate\n        }\n      }\n      "])), this.taskId)
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.data['task']; })).subscribe(function (data) {
            _this.task = data;
        });
    };
    TaskDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-detail',
            template: __webpack_require__(/*! ./task-detail.component.html */ "./src/app/task-detail/task-detail.component.html"),
            styles: [__webpack_require__(/*! ./task-detail.component.css */ "./src/app/task-detail/task-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], TaskDetailComponent);
    return TaskDetailComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/task-list/task-list.component.css":
/*!***************************************************!*\
  !*** ./src/app/task-list/task-list.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rhc2stbGlzdC90YXNrLWxpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/task-list/task-list.component.html":
/*!****************************************************!*\
  !*** ./src/app/task-list/task-list.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"all-content\">\n  <app-sidebar class=\"sticky\" id=\"sidebar-size\"></app-sidebar>\n  <div class=\"page-detail\">\n    <div class=\"project\">\n      <div class=\"project-title\">\n\n        <div style=\"display: flex; flex-flow: column; max-width: 100%;\">\n\n          <h4 class=\"home\">List of Task</h4>\n          \n          <div style=\"margin-top: 1% ;\">\n          <table class=\"table\" style=\"width: 90%;\">\n            <thead class=\"thead-dark\">\n              <tr>\n                <th scope=\"col\">#</th>\n                <th scope=\"col\">Project name</th>\n                <th scope=\"col\">From</th>\n                <th scope=\"col\">Status</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let task of tasks | async; let i = index;\" [routerLink]='\"/taskDetail/\" + task.id' style=\"cursor: pointer;\">\n                <th scope=\"row\">{{i}}</th>\n                <td>{{task.name}}</td>\n                <td>{{task.startDate}}</td>\n                <td>{{task.status}}</td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/task-list/task-list.component.ts":
/*!**************************************************!*\
  !*** ./src/app/task-list/task-list.component.ts ***!
  \**************************************************/
/*! exports provided: TaskListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListComponent", function() { return TaskListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var TaskListComponent = /** @class */ (function () {
    function TaskListComponent(apollo) {
        this.apollo = apollo;
    }
    TaskListComponent.prototype.ngOnInit = function () {
        this.tasks = this.apollo.watchQuery({
            query: graphql_tag__WEBPACK_IMPORTED_MODULE_3___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      query{\n        allTasks{\n          id\n          name\n          description\n          startDate\n          deadline\n          status\n          approve\n          taskCode\n          createdDate\n        }\n      }\n      "], ["\n      query{\n        allTasks{\n          id\n          name\n          description\n          startDate\n          deadline\n          status\n          approve\n          taskCode\n          createdDate\n        }\n      }\n      "])))
        })
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) { return result.data['allTasks']; }));
    };
    TaskListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list',
            template: __webpack_require__(/*! ./task-list.component.html */ "./src/app/task-list/task-list.component.html"),
            styles: [__webpack_require__(/*! ./task-list.component.css */ "./src/app/task-list/task-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], TaskListComponent);
    return TaskListComponent;
}());

var templateObject_1;


/***/ }),

/***/ "./src/app/user-service.service.ts":
/*!*****************************************!*\
  !*** ./src/app/user-service.service.ts ***!
  \*****************************************/
/*! exports provided: UserServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserServiceService", function() { return UserServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var UserServiceService = /** @class */ (function () {
    function UserServiceService(http) {
        this.http = http;
        this.process();
    }
    UserServiceService.prototype.ngOnInit = function () {
    };
    UserServiceService.prototype.getUser = function () {
        return this.curentUser;
    };
    UserServiceService.prototype.process = function () {
        var _this = this;
        this.http.get("user/me").subscribe(function (result) {
            _this.curentUser = result;
            console.log("curentUser", _this.curentUser);
        });
    };
    UserServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserServiceService);
    return UserServiceService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\ght\thesis\angular-apollo\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map